﻿using UnityEngine;
using System.Collections;

public class DeathHandler : MonoBehaviour {

	public PlayerStats playerStatsRef;
	public GameObject deathIndicatorRef;

	// Use this for initialization
	void Start () 
	{
		PlayerStats.OnDeath += DeathEventHandler;
	}

	private void DeathEventHandler()
	{
		Debug.Log("You are dead");
		deathIndicatorRef.SetActive(true);
	}

	void OnDestroy()
	{
		PlayerStats.OnDeath -= DeathEventHandler;
	}

	// Update is called once per frame
	void Update () {
	
	}
}

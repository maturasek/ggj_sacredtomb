﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameState : MonoBehaviour {

    public enum State {Waiting, Collecting, Drawing, StandBy, Dead};
    public State state;

    int[] fragmentsNeeded = { 4 };
    int level;
    int needFragmentNum;
    public int fragments;
    GameObject[] enemiesLeft;
    public Image triangle;
    public GameObject triangleColliders;
    int triangleCollidersCount = 29;
    public GameObject triangleSymbol;
    public GameObject[] spawnPoints;
    public GameObject enemyRed;
    bool spawnedW2;
    int wave2enemyCount = 5;

	void Start () {
        enemiesLeft = GameObject.FindGameObjectsWithTag("Enemy");
        spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        level = 1;
        fragments = 0;
        state = State.StandBy;
        spawnedW2 = false;

		state = State.Collecting;
		PlayerStats.OnDeath += () => 
		{
			state = State.Dead;
		};

		// spawn the 0 wave
		FindObjectOfType<EnemySpawner>().State = EnemySpawner.SpawnerStates.SPAWNING;
    }
	
    void SpawnEnemies(int enemyCount)
    {
        if (!spawnedW2)
        {
            for (int i = 0; i < enemyCount; i++)
            {
                int sp1 = Random.Range(1, 10);
                Instantiate(enemyRed, spawnPoints[i+1].transform.position, new Quaternion(0, 0, 0, 0));
            }
            spawnedW2 = true;
        }
    }

	void Update () {
		if (Input.GetKeyUp(KeyCode.F2))
		{
			Application.LoadLevel(0);
		}
		if (state == State.Dead)
		{
			return;
		}
        int enemiesAtThisLevel = GameObject.FindGameObjectsWithTag("Enemy").Length;

//        fragments = enemiesLeft.Length - enemiesAtThisLevel;
//
//        needFragmentNum = fragmentsNeeded[level - 1];
//
//        if (fragments < needFragmentNum && state != State.Waiting)
//        {
//            state = State.Collecting;
//        }
//        if (fragments == needFragmentNum)
//        {
//            state = State.Drawing;
//        }

        switch (state)
        {
			case State.StandBy:
				//triangle.enabled = true;
				break;
            case State.Collecting:
                //triangle.enabled = false;
				//triangleColliders.SetActive(false);
                break;
            case State.Drawing:
                triangleColliders.SetActive(true);
                int drawingColliderCount = GameObject.FindGameObjectsWithTag("DrawingCollider").Length;

                if (drawingColliderCount < triangleCollidersCount - 24)
                {
                    //triangleSymbol.SetActive(true);
                    //triangle.enabled = false;
                    //triangleColliders.SetActive(false);

                    //SpawnEnemies(wave2enemyCount);
                }
                else
                {
                    triangle.enabled = true;
                    //triangleSymbol.SetActive(false);
                }
                break;
        }

    }
}

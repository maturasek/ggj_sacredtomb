﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

	private static PlayerStats instance;
	public static PlayerStats Instance
	{
		get
		{
			if (instance == null)
			{
				instance = FindObjectOfType<PlayerStats>();
			}
			return instance;
		}
	}

	public int MaxHP;
	[SerializeField]
	private int _HP;
	public static int HP
	{
		get
		{
			return Instance._HP;
		}
		set
		{
			Instance._HP = Mathf.Clamp(value,0,Instance.MaxHP);
			if (Instance._HP == 0)
			{
				if (OnDeath != null)
				{
					OnDeath();
				}
			}
		}

	}

	public delegate void DeathHandler();
	public static event DeathHandler OnDeath;

    GameObject HPCircle;
    public float circleSize = 0.6f;

	[SerializeField]
	Vector3 StartScale;

	public float HPProgress;

	void Start () 
	{
        HPCircle = GameObject.Find("HPCircle");
	}
	
	void Update () {
		HPProgress =  (float)HP/(float)MaxHP;
		HPCircle.transform.localScale = Vector3.Lerp( Vector3.zero, StartScale,HPProgress);
    }
}

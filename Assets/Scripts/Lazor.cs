﻿using UnityEngine;
using System.Collections;

public class Lazor : MonoBehaviour {

    public LineRenderer lineRenderer;
    public GameObject laserEndpoint;
    public int range = 30;
    public static RaycastHit hit;
    Transform cross;

    public GameObject target;

    void Start()
    {
        cross = GameObject.Find("Cross").transform;
        transform.eulerAngles = new Vector3(70, 0, 0);
    }

    void Update()
    {
        lineRenderer.SetPosition(0, transform.position);

        transform.LookAt(cross.position);

        /*if (Input.GetMouseButtonDown(0))
        {
            if (target.tag == "Enemy")
            {
                Destroy(target);
            }
        }*/

        if (Physics.Raycast(transform.position, transform.forward, out hit, range))
        {
            lineRenderer.SetPosition(1, hit.point);

            target = hit.collider.gameObject;
        }
        else
        {
            lineRenderer.SetPosition(1, transform.position + transform.forward * range);
        }
        if (Input.GetMouseButton(0))
        {
            if (target.tag == "Enemy")
            {
				var enemyController = target.GetComponent<EnemyController>();
				enemyController.KillEnemy();
            }
            if (target.tag == "DrawingCollider")
            {
				var rootSymbol = target.transform.root.GetComponent<SymbolDrawing>();
				if (rootSymbol)
				{
					rootSymbol.setCollider(target.GetComponent<Collider>());
				}
            	//Destroy(target);
            }
        }
    }
}

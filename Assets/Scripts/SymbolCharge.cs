﻿using UnityEngine;
using System.Collections;

public class SymbolCharge : MonoBehaviour 
{

	public int MaxCharge = 5;
	public int Charge = 5;

	public SymbolDrawing symbolDrawingRef;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ActivateSymbol()
	{
		Charge = MaxCharge;
	}

	public void AttackSymbol(int damage)	
	{
		Charge -= damage;
		if (Charge == 0)
		{
			//symbolDrawingRef.gameObject.SetActive(true);
			symbolDrawingRef.State = SymbolDrawing.SymbolsState.Drawing;
			this.gameObject.SetActive(false);
		}


	}

	public void Recharge()
	{
		Charge = MaxCharge;
	}


}

﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	public enum EnemySymbol { TRIANGLE, SQUARE, CIRCLE }
	public EnemySymbol enemySymbol;

	public delegate void EnemyDeathHandler(EnemyController source);
	public event EnemyDeathHandler OnEnemyDeath;

    Transform target;
    public float speed = 10;
    bool canMove;

    void Start () {
        canMove = true;
        target = GameObject.Find("Player").transform;
	}
	
	void Update () {
        transform.LookAt(target);
        float step = speed * Time.deltaTime;

        if (canMove)
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);
	}

    void OnCollisionEnter(Collision col)
    {
        Debug.Log("Collided");
        canMove = false;
        if (col.transform.root.tag == enemySymbol.ToString())
        {
			col.gameObject.transform.root.GetComponent<SymbolCharge>().AttackSymbol(1);
			KillEnemy();
        }
		if (col.gameObject.GetComponent<PlayerStats>() !=  null)
		{

			PlayerStats.HP--;
			KillEnemy();
		}
    }

	void OnTriggerEnter(Collider other) {
		Debug.Log("Triggered");
		canMove = false;
		if (other.transform.root.tag == enemySymbol.ToString())
		{
			other.gameObject.transform.root.GetComponent<SymbolCharge>().AttackSymbol(1);
			KillEnemy();
		}
		if (other.gameObject.GetComponent<PlayerStats>() !=  null)
		{
			PlayerStats.HP--;
			KillEnemy();
		}
	}

	public void KillEnemy()
	{
		//Destroy only the collider, to play animation on the same object than destroy whenever it's comfy
		if (OnEnemyDeath != null)
		{
			OnEnemyDeath(this);
		}
		transform.localScale *= 0.5f;
		Destroy(this.GetComponent<Collider>());
		Destroy(this.GetComponent<Rigidbody>());
		Destroy(this.transform.gameObject,2f);
	}

	
}
